//
//  ShiftDetailsInteractor.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 10/03/2022.
//

import Foundation

protocol ShiftDetailsInteractor {
    func toogleState(shiftId: String)
    func getState(shiftId: String)
}

class ShiftDetailsInteractorImpl: ShiftDetailsInteractor {
    
    let state: ShiftDetailsState
    let repository: ShiftDetailsRepository
    
    init(state: ShiftDetailsState, repository: ShiftDetailsRepository) {
        self.state = state
        self.repository = repository
    }
    
    func toogleState(shiftId: String) {
        if self.state.isFavourite {
            self.repository.markShiftUnFavourite(shiftId: shiftId)
        } else {
            self.repository.markShiftFavourite(shiftId: shiftId)
        }
        self.state.isFavourite.toggle()
    }
    
    func getState(shiftId: String) {
        self.state.isFavourite = self.repository.getState(for: shiftId)
    }
}

//
//  ShiftDetailsState.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 10/03/2022.
//

import Foundation
import Combine

class ShiftDetailsState: ObservableObject {
    @Published var isFavourite: Bool = false
}

//
//  ShiftDetailsRepository.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 10/03/2022.
//

import Foundation

protocol ShiftDetailsRepository {
    func getState(for shiftId: String) -> Bool
    func markShiftFavourite(shiftId: String)
    func markShiftUnFavourite(shiftId: String)
}

class ShiftDetailsRepositoryImpl: ShiftDetailsRepository {
    
    func getState(for shiftId: String) -> Bool {
        return UserDefaults.standard.bool(forKey: shiftId)
    }
    
    func markShiftFavourite(shiftId: String) {
        UserDefaults.standard.set(true, forKey: shiftId)
    }
    
    func markShiftUnFavourite(shiftId: String) {
        UserDefaults.standard.set(false, forKey: shiftId)
    }
}

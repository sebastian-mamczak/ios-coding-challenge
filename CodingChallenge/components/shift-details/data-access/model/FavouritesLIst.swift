//
//  FavouritesLIst.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 10/03/2022.
//

import Foundation

struct FavouritesList: Codable {
    var list: [String] = []
}

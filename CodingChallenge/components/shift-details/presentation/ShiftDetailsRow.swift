//
//  ShiftDetailsRow.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 02/03/2022.
//

import SwiftUI

struct ShiftDetailsRow: View {
    
    let name: String
    let value: String
    var color: UIColor? = nil
    
    var body: some View {
        HStack {
            Text(self.name)
                .font(.system(size: 16, weight: .medium))
            Spacer()
            Text(self.value)
                .foregroundColor(Color(self.color ?? .systemGray))
        }
    }
}

//
//  ShiftDetailsView.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 02/03/2022.
//

import SwiftUI

struct ShiftDetailsView: View {
    
    @Environment(\.dismiss) var dismiss
    let shift: Shift
    
    @ObservedObject var state: ShiftDetailsState
    let interactor: ShiftDetailsInteractor
    
    private var content: some View {
        List {
            Section("Basic Info") {
                ShiftDetailsRow(name: "Identifier", value: "#\(self.shift.shiftId)")
                ShiftDetailsRow(name: "Kind", value: self.shift.shiftKind.rawValue)
                ShiftDetailsRow(name: "From", value: self.shift.normalizedStartDateTime.toString(format: "H:mm 'on' MM/dd/yy"))
                ShiftDetailsRow(name: "To", value: self.shift.normalizedEndDateTime.toString(format: "H:mm 'on' MM/dd/yy"))
                ShiftDetailsRow(name: "Within distance", value: "\(self.shift.withinDistance) miles")
            }
            Section("Extended Info") {
                ShiftDetailsRow(name: "Facility Type", value: self.shift.facilityType.name, color: self.shift.facilityType.color)
                ShiftDetailsRow(name: "Skill", value: self.shift.skill.name, color: self.shift.skill.color)
                ShiftDetailsRow(name: "Specialty", value: self.shift.localizedSpecialty.name, color: self.shift.localizedSpecialty.specialty.color)
                ShiftDetailsRow(name: "Premium rate", value: self.shift.premiumRate ? "Yes" : "No")
                ShiftDetailsRow(name: "Covid", value: self.shift.covid ? "Yes" : "No")
            }
            Section(content: {}, footer: {
                Button(self.state.isFavourite ? "Unfavourite" : "Favourite") {
                    print("Changed")
                    self.interactor.toogleState(shiftId: "\(shift.shiftId)")
                }.onAppear {
                    self.interactor.getState(shiftId: "\(shift.shiftId)")
                }
            })
            
        }
    }
    
    var body: some View {
        NavigationView {
            self.content
                .navigationTitle("Shift Details")
                .toolbar {
                    Button("Close") {
                        self.dismiss()
                    }
                }
        }
    }
}

struct ShiftDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let shift = Shift(
            shiftId: 2389361,
            startTime: "2022-03-07T00:00:00+00:00",
            endTime: "2022-03-07T12:00:00+00:00",
            normalizedStartDateTime: "2022-03-26 18:00:00".toDate(format: C.date.normalizedDateTimeFormat)!,
            normalizedEndDateTime: "2022-03-27 06:00:00".toDate(format: C.date.normalizedDateTimeFormat)!,
            timezone: "Central",
            premiumRate: true,
            covid: true,
            shiftKind: .evening,
            withinDistance: 10,
            facilityType: Shift.FacilityType(
                id: 2,
                name: "Skilled Nursing Facility",
                color: try! UIHexColor(hexString: "#AF52DE")
            ),
            skill: Shift.Skill(
                id: 2,
                name: "Long Term Care",
                color: try! UIHexColor(hexString: "#007AFF")
            ),
            localizedSpecialty: Shift.LocalizedSpecialty(
                id: 44,
                specialtyId: 6,
                stateId: 44,
                name: "Certified Nursing Aide",
                abbreviation: "CNA",
                specialty: Shift.LocalizedSpecialty.Speciality(
                    id: 6,
                    name: "Certified Nursing Aide",
                    color: try! UIHexColor(hexString: "#007AFF"),
                    abbreviation: "CNA"
                )
            )
        )
        let state = ShiftDetailsState()
        let repository = ShiftDetailsRepositoryImpl()
        let interactor = ShiftDetailsInteractorImpl(state: state, repository: repository)
        
        ShiftDetailsView(shift: shift, state: state, interactor: interactor)
    }
}

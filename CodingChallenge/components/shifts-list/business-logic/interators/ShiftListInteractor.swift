//
//  ShiftListInteractor.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 24/02/2022.
//

import Foundation
import SwiftUI

protocol ShiftListInteractor {
    func loadShifts()
    func loadNextShiftsIfNeeded(lastShift shift: Shift)
    func retryPreviousLoadRequest()
}

class ShiftListInteractorImpl {
    
    private var lastAfterDate: Date?
    let shiftListRepository: ShiftListRepository
    let shiftListState: ShiftListState
    
    init(shiftListRepository: ShiftListRepository, shiftListState: ShiftListState) {
        self.shiftListRepository = shiftListRepository
        self.shiftListState = shiftListState
    }
    
    @MainActor internal func startLoadingNextShifts() async {
        guard self.shiftListState.loadingState != .isLoading else {
            return
        }
        
        // update the state
        self.shiftListState.loadingState = .isLoading
        
        // fetch new items
        let nextAfterDate = self.lastAfterDate ?? Date()
        let result = await self.shiftListRepository.getWeeklyShifts(after: nextAfterDate)
        switch result {
            case .success(let list):
                print("Received items: \(list.count)")
                
                // save next date
                self.lastAfterDate = nextAfterDate.byAdding(day: C.date.numberOfDaysToAdd)
                
                // append new data
                self.shiftListState.shifts.append(contentsOf: list)
                
                // change state
                self.shiftListState.loadingState = .done
            case .failure(let apiError):
                self.shiftListState.loadingState = .error(apiError.localizedDescription)
        }
    }
}

extension ShiftListInteractorImpl: ShiftListInteractor {
    
    func loadShifts() {
        Task {
            await self.startLoadingNextShifts()
        }
    }
    
    func loadNextShiftsIfNeeded(lastShift shift: Shift) {
        let index = self.shiftListState.shifts.lastIndex(of: shift)
        guard let index = index else { return }
        
        let threshold = 5
        if (index + threshold) >= self.shiftListState.shifts.count {
            self.loadShifts()
        }
    }
    
    func retryPreviousLoadRequest() {
        self.loadShifts()
    }
}

//
//  ShiftListState.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 24/02/2022.
//

import Foundation

class ShiftListState: ObservableObject {
    @Published var shifts: [Shift] = []
    @Published var loadingState: LoadingState = .done
}

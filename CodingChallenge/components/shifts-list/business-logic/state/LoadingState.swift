//
//  LoadingState.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 27/02/2022.
//

import Foundation

enum LoadingState: Equatable {
    case isLoading
    case done
    case error(String)
}

//
//  LoaderView.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 24/02/2022.
//

import SwiftUI

struct LoaderView: View {
    
    @Binding var state: LoadingState
    var reloadOnErrorCallback: Callback? = nil
    @State private var isAnimating = false
    
    private let circleItemsCount: Int = 3
    private let circleItemLength: Double = 0.12
    private var frameSize: CGSize {
        return CGSize(width: 60, height: 60)
    }
    private var circleItemsStartingPoints: [Double] {
        return (0 ..< self.circleItemsCount)
            .map { Double($0) / Double(self.circleItemsCount) }
    }
    private var circleItemStrokeStyle: StrokeStyle {
        return StrokeStyle(lineWidth: 5, lineCap: .round)
    }
    private var circleItemRotationAngle: Angle {
        return Angle(degrees: self.isAnimating ? 360 : 0)
    }
    private var circleItemAnimation: Animation {
        if isAnimating == false {
            return Animation.default
        }
        return Animation.linear(duration: 1).repeatForever(autoreverses: false)
    }
    
    func onReloadRequest(_ callback: @escaping Callback) -> some View {
        LoaderView(state: self.$state, reloadOnErrorCallback: callback)
    }
    
    var body: some View {
        HStack {
            Spacer()
            switch self.state {
                case .error(let message):
                    VStack {
                        Text(message)
                            .foregroundColor(.red)
                            .multilineTextAlignment(.center)
                            .padding()
                        Button("Tap to reload") {
                            self.reloadOnErrorCallback?()
                        }
                    }
                case .isLoading:
                    ZStack {
                        Circle()
                            .stroke(Color(.systemGray5), lineWidth: 10)
                            .frame(size: self.frameSize)
                        ForEach(self.circleItemsStartingPoints, id: \.self) { startingPoint in
                            Circle()
                                .trim(from: startingPoint, to: startingPoint + self.circleItemLength)
                                .stroke(Color.green, style: self.circleItemStrokeStyle)
                                .frame(size: self.frameSize)
                                .rotationEffect(self.circleItemRotationAngle)
                                .animation(self.circleItemAnimation, value: self.isAnimating)
                        }
                    }.onAppear() {
                        self.isAnimating = true
                    }.onDisappear() {
                        self.isAnimating = false
                    }
                case .done:
                    ZStack {
                        Rectangle()
                            .fill(.clear)
                            .frame(size: self.frameSize)
                        Circle()
                            .fill(.green)
                            .frame(size: self.frameSize)
                            .scaleEffect(0.15)
                    }
            }
            Spacer()
        }
    }
}

struct LoaderView_Previews: PreviewProvider {
    static var previews: some View {
        LoaderView(state: .constant(.error("There was an error")))
        LoaderView(state: .constant(.isLoading))
        LoaderView(state: .constant(.done))
    }
}

//
//  ShiftRowDetailsLabel.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 01/03/2022.
//

import SwiftUI

struct ShiftRowDetailsLabel: View {
    
    let image: String
    let text: String
    let color: UIColor
    
    var body: some View {
        HStack(spacing: 8) {
            Image(image)
                .renderingMode(.template)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 16.0, height: 16.0)
                .foregroundColor(Color(self.color))
            Text(text)
                .foregroundColor(Color(self.color))
                .font(.system(size: 16))
                .lineLimit(1)
        }
    }
}

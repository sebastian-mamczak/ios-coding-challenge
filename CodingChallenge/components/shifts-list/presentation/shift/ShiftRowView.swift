//
//  ShiftRowView.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 27/02/2022.
//

import SwiftUI

struct ShiftRowView: View {
    
    let shift: Shift
    
    var body: some View {
        VStack {
            HStack(alignment: .top, spacing: 12) {
                ShiftRowTimeView(shift: shift)
                ShiftRowDetails(shift: shift)
            }
        }.padding(EdgeInsets(top: 4, leading: 12, bottom: 4, trailing: 12))
    }
}

struct ShiftRowView_Previews: PreviewProvider {
    static var previews: some View {
        let shift = Shift(
            shiftId: 2389361,
            startTime: "2022-03-07T00:00:00+00:00",
            endTime: "2022-03-07T12:00:00+00:00",
            normalizedStartDateTime: "2022-03-26 18:00:00".toDate(format: C.date.normalizedDateTimeFormat)!,
            normalizedEndDateTime: "2022-03-27 06:00:00".toDate(format: C.date.normalizedDateTimeFormat)!,
            timezone: "Central",
            premiumRate: true,
            covid: true,
            shiftKind: .evening,
            withinDistance: 10,
            facilityType: Shift.FacilityType(
                id: 2,
                name: "Skilled Nursing Facility",
                color: try! UIHexColor(hexString: "#AF52DE")
            ),
            skill: Shift.Skill(
                id: 2,
                name: "Long Term Care",
                color: try! UIHexColor(hexString: "#007AFF")
            ),
            localizedSpecialty: Shift.LocalizedSpecialty(
                id: 44,
                specialtyId: 6,
                stateId: 44,
                name: "Certified Nursing Aide",
                abbreviation: "CNA",
                specialty: Shift.LocalizedSpecialty.Speciality(
                    id: 6,
                    name: "Certified Nursing Aide",
                    color: try! UIHexColor(hexString: "#007AFF"),
                    abbreviation: "CNA"
                )
            )
        )
        ShiftRowView(shift: shift)
            .previewDevice("iPhone 13")
    }
}

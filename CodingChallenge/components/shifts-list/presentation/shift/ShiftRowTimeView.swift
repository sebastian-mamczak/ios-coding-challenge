//
//  ShiftRowTimeView.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 01/03/2022.
//

import SwiftUI

struct ShiftRowTimeView: View {
    
    let shift: Shift
    
    private var fgColor: Color {
        switch self.shift.shiftKind {
            case .day:
                return Color(try! UIHexColor(hexString: "#000000"))
            case .evening:
                return Color(try! UIHexColor(hexString: "#000000"))
            case .night:
                return Color(try! UIHexColor(hexString: "#ffffff"))
        }
    }
    private var bgColor: Color {
        switch self.shift.shiftKind {
            case .day:
                return Color(try! UIHexColor(hexString: "#F4EE79"))
            case .evening:
                return Color(try! UIHexColor(hexString: "#E8B05D"))
            case .night:
                return Color(try! UIHexColor(hexString: "#175F99"))
        }
    }
    
    var body: some View {
        VStack(spacing: 0) {
            Text(shift.normalizedStartDateTime.toString(format: "d MMM"))
                .padding(8)
            HLine()
                .background(self.fgColor)
            VStack {
                Text(shift.normalizedStartDateTime.toString(format: "HH:mm"))
                Text(shift.normalizedEndDateTime.toString(format: "HH:mm"))
            }
            .padding(8)
        }
        .font(.system(size: 16, weight: .bold, design: .monospaced))
        .foregroundColor(self.fgColor)
        .frame(width: 80)
        .background(
            RoundedRectangle(cornerRadius: 5)
                .fill(self.bgColor)
        )
    }
}

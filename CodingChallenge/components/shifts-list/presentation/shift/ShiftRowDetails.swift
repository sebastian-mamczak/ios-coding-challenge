//
//  ShiftRowDetails.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 01/03/2022.
//

import SwiftUI

struct ShiftRowDetails: View {
    
    let shift: Shift
    
    private var covidColor: UIColor {
        return try! UIHexColor(hexString: "#36DA1C")
    }
    private var premiumColor: UIColor {
        return try! UIHexColor(hexString: "#F116F4")
    }
    private func getPlace() -> String {
        return "\(self.shift.facilityType.name), \(self.shift.withinDistance) mi."
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 4) {
            VStack(alignment: .leading, spacing: 8) {
                ShiftRowDetailsLabel(image: "place",
                                    text: self.getPlace(),
                                    color: self.shift.facilityType.color)
                HLine()
                ShiftRowDetailsLabel(image: "skill",
                                    text: self.shift.skill.name,
                                    color: self.shift.skill.color)
            }
            HStack() {
                Spacer()
                if self.shift.covid {
                    TagView(text: "COVID", color: self.covidColor)
                }
                if self.shift.premiumRate {
                    TagView(text: "PREMIUM", color: self.premiumColor)
                }
            }
        }
        .padding(.top, 8.0)
    }
}

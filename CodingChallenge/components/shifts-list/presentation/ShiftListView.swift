//
//  ShiftListView.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 26/02/2022.
//

import SwiftUI

struct ShiftListView: View {
    
    @ObservedObject var state: ShiftListState
    let interactor: ShiftListInteractor
    
    @State private var selectedShift: Shift?
    
    var body: some View {
        ScrollView() {
            LazyVStack {
                ForEach(self.state.shifts, id: \.self) { shift in
                    ShiftRowView(shift: shift)
                        .onAppear {
                            self.interactor.loadNextShiftsIfNeeded(lastShift: shift)
                        }
                        .sheet(item: self.$selectedShift) { selectedShift in
                            let state = ShiftDetailsState()
                            let repository = ShiftDetailsRepositoryImpl()
                            let interactor = ShiftDetailsInteractorImpl(state: state, repository: repository)
                            
                            ShiftDetailsView(shift: selectedShift, state: state, interactor: interactor)
                        }
                        .onTapGesture {
                            self.selectedShift = shift
                        }
                }
                LoaderView(state: self.$state.loadingState)
                    .onReloadRequest {
                        self.interactor.retryPreviousLoadRequest()
                    }
            }
        }.onAppear {
            self.interactor.loadShifts()
        }
    }
}

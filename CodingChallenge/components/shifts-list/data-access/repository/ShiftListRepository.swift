//
//  ShiftListRepository.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 23/02/2022.
//

import Foundation

protocol ShiftListRepository {
    func getWeeklyShifts(after date: Date) async -> WeeklyShiftsResult
}

class ShiftListRepositoryImpl {
    let apiService: ShiftListApiService
    
    init(apiService: ShiftListApiService) {
        self.apiService = apiService
    }
}

extension ShiftListRepositoryImpl: ShiftListRepository {
    
    func getWeeklyShifts(after date: Date) async -> WeeklyShiftsResult {
        do {
            let list = try await self.apiService.fetchShifts(
                start: date.toString(format: C.date.dayListFormat),
                type: C.api.listType,
                radius: C.api.searchRadius).data
            let flatList = list
                .flatMap { $0.shifts }
            return WeeklyShiftsResult.success(flatList)
        } catch(let apiError as ApiServiceTypes.ApiError) {
            return WeeklyShiftsResult.failure(apiError)
        } catch {
            return WeeklyShiftsResult.failure(.networkProblem("Unexpected error"))
        }
    }
}

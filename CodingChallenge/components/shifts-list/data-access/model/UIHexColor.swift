//
//  UIHexColor.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 23/02/2022.
//

import UIKit

class UIHexColor: UIColor, Decodable {
    
    public required convenience init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let hexString = try container.decode(String.self)
        try self.init(hexString: hexString)
    }
    
    public convenience init(hexString: String) throws {
        // sanity check
        guard hexString.hasPrefix("#") && hexString.count == 7 else {
            throw DecodingError.dataCorrupted(.init(
                codingPath: [],
                debugDescription: "Invalid color hex string",
                underlyingError: nil))
        }
        
        // scan hex value into
        let hexValue = String(hexString.suffix(6))
        let scanner = Scanner(string: hexValue)
        var hexNumber: UInt64 = 0
        guard scanner.scanHexInt64(&hexNumber) else {
            throw DecodingError.dataCorrupted(.init(
                codingPath: [],
                debugDescription: "Cannot parse color hex string",
                underlyingError: nil))
        }
        
        // create color using RGB
        self.init(
            red: CGFloat((hexNumber & 0xff0000) >> 16) / 255,
            green: CGFloat((hexNumber & 0x00ff00) >> 8) / 255,
            blue: CGFloat(hexNumber & 0x0000ff) / 255,
            alpha: 1.0)
    }
}

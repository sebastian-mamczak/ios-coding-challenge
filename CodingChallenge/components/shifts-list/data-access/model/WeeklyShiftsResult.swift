//
//  WeeklyShiftsResult.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 23/02/2022.
//

import Foundation

typealias WeeklyShiftsResult = Result<[Shift], ApiServiceTypes.ApiError>

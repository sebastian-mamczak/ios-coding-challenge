//
//  DailyShiftsList.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 20/02/2022.
//

import Foundation

struct DailyShiftsList: Decodable, Hashable {
    var date: Date
    var shifts: [Shift]
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(date)
    }
    
    static func == (lhs: DailyShiftsList, rhs: DailyShiftsList) -> Bool {
        return lhs.date == rhs.date
    }
}

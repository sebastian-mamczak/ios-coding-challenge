//
//  Shift.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 20/02/2022.
//

import Foundation

struct Shift: Decodable {
    let shiftId: Int
    let startTime: String
    let endTime: String
    let normalizedStartDateTime: Date
    let normalizedEndDateTime: Date
    let timezone: String
    let premiumRate: Bool
    let covid: Bool
    let shiftKind: ShiftKind
    let withinDistance: Int
    let facilityType: FacilityType
    let skill: Skill
    let localizedSpecialty: LocalizedSpecialty
}

extension Shift: Identifiable {
    var id: Int {
        return shiftId
    }
}

extension Shift: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.shiftId)
    }
    
    static func == (lhs: Shift, rhs: Shift) -> Bool {
        return lhs.shiftId == rhs.shiftId
    }
}

extension Shift {
    enum ShiftKind: String, Decodable {
        case day = "Day Shift"
        case evening = "Evening Shift"
        case night = "Night Shift"
    }
    struct FacilityType: Decodable{
        let id: Int
        let name: String
        let color: UIHexColor
    }
    struct Skill: Decodable {
        let id: Int
        let name: String
        let color: UIHexColor
    }
    struct LocalizedSpecialty: Decodable {
        let id: Int
        let specialtyId: Int
        let stateId: Int
        let name: String
        let abbreviation: String
        let specialty: Speciality
    }
}

extension Shift.LocalizedSpecialty {
    struct Speciality: Decodable {
        let id: Int
        let name: String
        let color: UIHexColor
        let abbreviation: String
    }
}

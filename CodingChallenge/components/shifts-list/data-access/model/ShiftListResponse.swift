//
//  ShiftListResponse.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 20/02/2022.
//

import Foundation

struct ShiftListResponse: Decodable {
    let data: [DailyShiftsList]
}

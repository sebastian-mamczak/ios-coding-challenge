//
//  ShiftListApiService.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 22/02/2022.
//

import Foundation

protocol ShiftListApiService {
    func fetchShifts(start: String, type: String, radius: Int) async throws -> ShiftListResponse
}

actor ShiftListApiServiceImpl: ApiService, ShiftListApiService {
    
    let decoder: JSONDecoder
    var session: URLSession
    
    init(decoder: JSONDecoder, config: URLSessionConfiguration) {
        self.decoder = decoder
        self.session = URLSession(configuration: config)
    }
    
    func fetchShifts(start: String, type: String, radius: Int) async throws -> ShiftListResponse {
        let requestDef = ApiServiceTypes.RequestDefinition(
            urlString: C.api.urlString,
            method: "GET",
            headers: C.api.jsonHeaders,
            queryParams: [
                "address": C.api.searchAddress,
                "start": start,
                "type": type,
                "radius": String(radius)
            ],
            body: nil,
            decoder: self.decoder,
            accepptedResponseCodes: [C.http.statusCodes.ok])
        return try await self.sendReqest(requestDef, using: self.session)
    }
}

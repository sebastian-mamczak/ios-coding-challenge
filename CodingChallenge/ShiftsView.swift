//
//  ShiftsView.swift
//  CodingChallenge
//
//  Created by Brady Miller on 4/7/21.
//

import SwiftUI

struct ShiftsView: View {
    
    var state: ShiftListState = ShiftListState()
    
    func getInteractor() -> ShiftListInteractor {
        let apiService = ShiftListApiServiceImpl(
            decoder: C.api.decoder,
            config: C.api.sessionConfiguration)
        let interactor = ShiftListInteractorImpl(
            shiftListRepository: ShiftListRepositoryImpl(apiService: apiService),
            shiftListState: self.state)
        return interactor
    }
    
    var body: some View {
        NavigationView {
            ShiftListView(state: self.state, interactor: self.getInteractor())
                .navigationTitle("Shifts")
        }
    }
}

struct ShiftsView_Previews: PreviewProvider {
    static var previews: some View {
        ShiftsView()
    }
}

//
//  Constants.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 21/02/2022.
//

import Foundation

typealias C = Constants

enum Constants {
    enum api {
        static let urlString: String = "https://staging-app.shiftkey.com/api/v2/available_shifts"
        static let searchAddress: String = "Dallas, TX"
        static let listType: String = "week"
        static let searchRadius: Int = 35
        static let jsonHeaders: [String: String] = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        static let decoder: JSONDecoder = {
            let dateFormats = [
                C.date.dayListFormat,
                C.date.normalizedDateTimeFormat
            ]
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .custom { decoder in
                let container = try decoder.singleValueContainer()
                let stringDate = try container.decode(String.self)
                for dateFormat in dateFormats {
                    if let date = stringDate.toDate(format: dateFormat) {
                        return date
                    }
                }
                throw DecodingError.dataCorruptedError(in: container, debugDescription: "Cannot decode date: \"\(stringDate)\"")
            }
            return decoder
        }()
        static let sessionConfiguration: URLSessionConfiguration = {
            var config = URLSessionConfiguration.default
            config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
            config.timeoutIntervalForRequest = 20.0
            config.allowsCellularAccess = true
            return config
        }()
    }
    enum date {
        static let dayListFormat: String = "yyyy-MM-dd"
        static let normalizedDateTimeFormat: String = "yyyy-MM-dd HH:mm:ss"
        static let numberOfDaysToAdd: Int = 7
    }
    enum http {
        enum statusCodes {
            static let ok: Int = 200
        }
    }
}

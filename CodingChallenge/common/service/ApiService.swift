//
//  ApiService.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 21/02/2022.
//

import Foundation

protocol ApiService {}

extension ApiService {
    
    func sendReqest<T>(_ requestDef: ApiServiceTypes.RequestDefinition, using session: URLSession) async throws -> T where T: Decodable {
        do {
            let request = try requestDef.createRequest()
            print("Sending request", request)
            let (data, response) = try await session.data(for: request)
            guard let httpResponse = response as? HTTPURLResponse else {
                throw ApiServiceTypes.ApiError.networkProblem("No HTTP response was found")
            }
            guard  requestDef.accepptedResponseCodes.contains(httpResponse.statusCode) else {
                throw ApiServiceTypes.ApiError.unacceptedResponseCode(httpResponse.statusCode)
            }
            return try requestDef.decoder.decode(T.self, from: data)
        } catch(let apiError as ApiServiceTypes.ApiError) {
            // rethrow
            throw apiError
        } catch(let decodingError as DecodingError) {
            throw ApiServiceTypes.ApiError.cannotDecodeResponse(String(describing: decodingError))
        } catch(let nsError as NSError) {
            // found specific error
            if nsError.domain == NSURLErrorDomain && nsError.code == NSURLErrorNotConnectedToInternet {
                throw ApiServiceTypes.ApiError.notConnected
            } else {
                throw ApiServiceTypes.ApiError.networkProblem(nsError.localizedDescription)
            }
        }
    }
}

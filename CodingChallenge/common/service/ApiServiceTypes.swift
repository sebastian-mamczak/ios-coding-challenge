//
//  ApiServiceTypes.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 23/02/2022.
//

import Foundation

enum ApiServiceTypes {
    enum ApiError: Error {
        case cannotCreateRequest
        case notConnected
        case unacceptedResponseCode(Int)
        case networkProblem(String)
        case cannotDecodeResponse(String)
    }
    struct RequestDefinition {
        let urlString: String
        let method: String
        let headers: [String: String]?
        let queryParams: [String: String]?
        let body: Data?
        let decoder: JSONDecoder
        let accepptedResponseCodes: [Int]
    }
}

extension ApiServiceTypes.ApiError: LocalizedError, CustomStringConvertible {
    var description: String {
        return self.errorDescription ?? "Error"
    }
    var errorDescription: String? {
        switch self {
            case .cannotCreateRequest:
                return "Cannot create request"
            case .notConnected:
                return "Not connected to Internet"
            case .unacceptedResponseCode(let code):
                return "Unaccepted response code: \(code)"
            case .networkProblem(let reason):
                return "Network problem: \(reason)"
            case .cannotDecodeResponse(let reason):
                return "Cannot decode the response: \(reason)"
        }
    }
}

extension ApiServiceTypes.RequestDefinition {
    func createRequest() throws -> URLRequest {
        guard var components = URLComponents(string: self.urlString) else {
            throw ApiServiceTypes.ApiError.cannotCreateRequest
        }
        components.queryItems = self.queryParams?.map { name, value in
            URLQueryItem(name: name, value: value)
        }
        guard let url = components.url else {
            throw ApiServiceTypes.ApiError.cannotCreateRequest
        }
        var request = URLRequest(url: url)
        request.httpMethod = self.method
        request.allHTTPHeaderFields = self.headers
        request.httpBody = self.body
        return request
    }
}

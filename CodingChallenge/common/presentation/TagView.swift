//
//  TagView.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 01/03/2022.
//

import SwiftUI

struct TagView: View {
    
    let text: String
    let color: UIColor
    
    private var backgroundView: some View {
        RoundedRectangle(cornerRadius: 15)
            .fill(Color(self.color))
    }
    
    var body: some View {
        HStack(spacing: 4) {
            Circle()
                .frame(width: 10, height: 10)
            Text(text)
                .foregroundColor(.black)
                .font(.system(size: 13))
        }
        .padding(4)
        .background(self.backgroundView)
    }
}

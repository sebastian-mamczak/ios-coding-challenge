//
//  HLine.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 01/03/2022.
//

import SwiftUI

struct HLine: View {
    var body: some View {
        Rectangle()
            .padding(0)
            .frame(minWidth: 1, maxWidth: .infinity, minHeight: 1, maxHeight: 1)
            .background(.black)
    }
}

//
//  Types.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 25/02/2022.
//

import Foundation

typealias Callback = () -> ()

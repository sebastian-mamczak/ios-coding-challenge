//
//  String.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 01/03/2022.
//

import Foundation

extension String {
    func toDate(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}

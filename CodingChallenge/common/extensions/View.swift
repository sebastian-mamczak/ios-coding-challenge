//
//  View.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 25/02/2022.
//

import SwiftUI

extension View {
    func frame(size: CGSize) -> some View {
        return self.frame(width: size.width, height: size.height, alignment: .center)
    }
}

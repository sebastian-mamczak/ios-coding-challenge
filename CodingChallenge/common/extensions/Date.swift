//
//  Date.swift
//  CodingChallenge
//
//  Created by Sebastian Mamczak on 21/02/2022.
//

import Foundation

extension Date {
    func byAdding(day: Int) -> Date {
        var dateComponent = DateComponents()
        dateComponent.day = day
        guard let newDate = Calendar.current.date(byAdding: dateComponent, to: self) else {
            fatalError("Cannot add days")
        }
        return newDate
    }
    
    func toString(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
